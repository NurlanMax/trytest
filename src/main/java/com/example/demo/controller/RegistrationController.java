package com.example.demo.controller;

import com.example.demo.models.Person;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    @Autowired
    private UserService userService;
    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("personForm",new Person());
        return "registration";
    }
    @PostMapping("/registration")
    public String addPerson(@ModelAttribute("personForm") @Valid Person personForm, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()) {
            return "registration";
        }
        if (!personForm.getPassword().equals(personForm.getPasswordConfirm())) {
            model.addAttribute("passwordError","passwords havent matched");
            return "registration";
        }
        if(!userService.saveUser(personForm)) {
            model.addAttribute("personNameError", "user with this name already exists");
            return "registration";
        }
        return "redirect:/";
    }
}
