package com.example.demo.controller;

import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AdminController {
    @Autowired
    private UserService userService;
    @GetMapping("/admin")
    public String peopleList(Model model) {
        model.addAttribute("allPeople", userService.allPeople());
        return "admin";
    }
    @PostMapping("/admin")
    public String deletePerson(@RequestParam(required = true, defaultValue = "")Long personId,
                               @RequestParam(required = true, defaultValue = "")String action, Model model) {
        if(action.equals("delete")) {
            userService.deletePerson(personId);
        }
        return "redirect:/admin";
    }
    @GetMapping("/admin/gt/{personId}")
    public String gtPerson(@PathVariable("personId") Long personId, Model model) {
        model.addAttribute("allPeople",userService.persongtList(personId));
        return "admin";
    }
}
