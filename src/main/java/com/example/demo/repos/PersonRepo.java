package com.example.demo.repos;

import com.example.demo.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepo  extends JpaRepository<Person, Long> {
    Person findByName(String name);
}
