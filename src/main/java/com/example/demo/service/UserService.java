package com.example.demo.service;

import com.example.demo.models.Person;
import com.example.demo.models.Role;
import com.example.demo.repos.PersonRepo;
import com.example.demo.repos.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    PersonRepo personRepo;
    @Autowired
    RoleRepo roleRepo;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Person person = personRepo.findByName(name);

        if (person==null) {
            throw  new UsernameNotFoundException("Person not found");
        }
        return person;
    }
    public Person findPersonById(Long personId) {
        Optional<Person> personFromDb=personRepo.findById(personId);
        return personFromDb.orElse(new Person());
    }
    public List<Person> allPeople() {
        return  personRepo.findAll();
    }
    public boolean saveUser(Person person) {
        Person personFromDb=personRepo.findByName(person.getName());
        if (personFromDb!=null) {
            return false;
        }
        person.setRoles(Collections.singleton(new Role(1L,"ROLE_USER")));
        person.setPassword(bCryptPasswordEncoder.encode(person.getPassword()));
        personRepo.save(person);
        return true;
    }
    public boolean deletePerson(Long id) {
        if(personRepo.findById(id).isPresent()) {
            personRepo.deleteById(id);
            return true;
        }
        return false;
    }
    public List<Person> persongtList(Long idMin) {
        return em.createQuery("SELECT p FROM Person p WHERE p.id > :paramId", Person.class)
                .setParameter("paramId", idMin).getResultList();
    }
}
